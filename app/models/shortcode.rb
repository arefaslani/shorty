require 'sinatra/activerecord'

class Shortcode < ActiveRecord::Base
  scope :used, -> { where(used: true)}
  scope :unused, -> { where(used: false)}
end
