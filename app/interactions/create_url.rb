require 'active_interaction'
require 'securerandom'

require_relative '../models/url'
require_relative '../models/shortcode'

class CreateUrl < ActiveInteraction::Base
  string :shortcode, default: nil
  string :original_url

  validates :shortcode,
            format: {
              with: /\A[0-9a-zA-Z_]{4,}\z/,
              message: 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$'
            },
            allow_nil: true

  def execute
    res = Url.new(
      shortcode: fetch_new_shortcode(shortcode).code,
      original_url: original_url
    )

    return errors.merge!(res.errors) unless res.save

    res
  rescue ActiveRecord::RecordNotUnique
    errors.add(:shortcode, :taken)
  end

  private

  def fetch_new_shortcode(code = nil)
    return Shortcode.create(code: code, used: true) if code

    Shortcode.transaction do
      shortcode = Shortcode.lock.unused.order("RANDOM()").first
      shortcode.toggle! :used
      shortcode
    end
  end
end
