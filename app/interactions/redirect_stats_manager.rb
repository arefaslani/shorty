require 'active_interaction'

require_relative '../models/url'

class RedirectStatsManager < ActiveInteraction::Base
  string :shortcode

  def execute
    Url.transaction do
      url = Url.find_by!(shortcode: shortcode).lock!
      url.increment!(:redirect_count)
      url
    end
  rescue ActiveRecord::RecordNotFound
    errors.add(:shortcode, :invalid, message: 'The shortcode cannot be found in the system')
  end
end
