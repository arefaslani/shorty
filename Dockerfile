FROM ruby:2.6.5-alpine

RUN apk add --no-cache --update build-base \
                                linux-headers \
                                postgresql-dev \
                                tzdata

RUN mkdir /app

WORKDIR /app

RUN gem install bundler

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install

COPY . /app

EXPOSE 3000
