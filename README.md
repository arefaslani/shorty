Shorty URL shortner
=====================

## How it works
Unlike many implementations on the web that utilize a loop for generating a unique shortcode, in this implementation I prepopulated database with all possible combinations with those 6 alphanumerical characters (for this example application I created 300 combinations in db seeds). Suppose we can have utmost 50 billion combinations with those 6 characters. When there are already many entries in the database, the chance of collision will get higher dramatically and the performance will get lower. But with this implementation, we will choose an unused shortcode from the database and even if 90% of the shortcodes are already in use, there is no performance issue. I also used database locks for updating entries, because suppose two clients click on a URL at the same time, then without using locks, maybe the `redirect_count` will be increased by 1 which is a bug.

## Running in production
For starting the server in production mode just run

```bash
docker-compose up -d
```

For the first run, you also need to setup the database

```bash
docker-compose run -e RACK_ENV=production --rm api rake db:setup
```

## Running in development
For starting the application in development mode, first you should create
a user and set the `DATABASE_USERNAME` and `DATABASE_PASSWORD` variables.
For the first run, you should also setup the database with

```bash
rake db:setup
```

Then you can start the server with

```bash
unicorn -c /app/config/unicorn.rb -E development
```

## Running tests

You can either run the tests in development mode with just `RACK_ENV=test rspec`.
For running tests on docker stack, at first run you should setup test database
with

```bash
docker-compose run -e RACK_ENV=test --rm api rake db:setup
```

and then you can run tests with

```bash
docker-compose run -e RACK_ENV=test --rm api rspec
```
