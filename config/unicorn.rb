# set path to app that will be used to configure unicorn,
# note the trailing slash in this example
APP_ROOT = File.expand_path File.join(File.dirname(__FILE__), '../')

worker_processes 2
working_directory APP_ROOT

timeout 30

# Specify path to socket unicorn listens to,
# we will use this in our nginx.conf later
listen '0.0.0.0:3000'
