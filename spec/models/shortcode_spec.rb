require_relative '../spec_helper'
require_relative '../../app/models/shortcode'

describe Shortcode, type: :model do
  before do
    create_list(:shortcode, 2, used: true)
    create_list(:shortcode, 3, used: false)
  end

  it 'has a valid factory' do
    expect(build(:shortcode)).to be_valid
  end

  context 'when code is duplicate' do
    before { create(:shortcode, code: '123456') }
    it 'raises not unique exception' do
      expect { create(:shortcode, code: '123456') }
        .to raise_error(ActiveRecord::RecordNotUnique)
    end
  end

  describe '.used' do
    it 'only returns used shortcodes' do
      expect(described_class.used.count).to eq 2
    end
  end

  describe '.unused' do
    it 'only returns unused shortcodes' do
      expect(described_class.unused.count).to eq 3
    end
  end
end
