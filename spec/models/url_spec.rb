require_relative '../spec_helper'
require_relative '../../app/models/url'

describe Url, type: :model do
  it 'has a valid factory' do
    expect(build(:url)).to be_valid
  end

  describe 'shortcode' do
    it 'is unique' do
      create(:url, shortcode: '123456')
      expect { create(:url, shortcode: '123456') }
        .to raise_error ActiveRecord::RecordNotUnique
    end

    it 'is case sensitive' do
      create(:url, shortcode: 'abcdef')
      expect(create(:url, shortcode: 'ABCDEF')).to be_valid
    end
  end
end
