require 'rack/test'
require 'rspec'
require 'factory_bot'
require 'sinatra/activerecord'
require 'database_cleaner'
require 'rspec/json_expectations'

require_relative '../app'

# Set test environment
ENV['RACK_ENV'] = 'test'

# Load application
require File.expand_path File.join(File.dirname(__FILE__), '../app')

# Turn off active record logs in tests
ActiveRecord::Base.logger = nil

# Setup FactoryBot
RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    FactoryBot.definition_file_paths = %w{./factories ./test/factories ./spec/factories}
    FactoryBot.find_definitions
  end

  # Configure database cleaner to truncate database before running tests
  DatabaseCleaner.strategy = :truncation
  config.before(:all) do
    DatabaseCleaner.clean
  end
  config.after(:each) do
    DatabaseCleaner.clean
  end
end

module RSpecMixin
  include Rack::Test::Methods
  def app() App end
end

RSpec.configure { |c| c.include RSpecMixin }
