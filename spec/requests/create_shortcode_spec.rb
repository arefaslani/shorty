require_relative '../spec_helper'

RSpec.describe 'Shortcode creation', :type => :request do
  context 'with valid params' do
    let(:valid_params) { attributes_for(:url) }

    before do
      post '/shorten', {
        url: valid_params.fetch(:original_url),
        shortcode: valid_params.fetch(:shortcode)
      }.to_json
    end

    it 'responds with created' do
      expect(last_response.status).to eq 201
    end

    it 'responds with JSON format' do
      expect(last_response.content_type).to eq("application/json")
    end

    it 'has a valid JSON body' do
      expect(last_response.body).to include_json({
        shortcode: valid_params.fetch(:shortcode)
      })
    end
  end

  context 'when providing preferred shortcode' do
    before do
      post '/shorten', {
        url: attributes_for(:url).fetch(:original_url),
        shortcode: 'ABcdEF',
      }.to_json
    end

    it 'responds with created' do
      expect(last_response.status).to eq 201
    end

    it 'responds with the provided shortcode' do
      expect(last_response.body).to include_json({ shortcode: 'ABcdEF' })
    end

    context 'when shortcode contains invalid characters' do
      before do
        post '/shorten', {
          url: attributes_for(:url).fetch(:original_url),
          shortcode: 'ABcdE!',
        }.to_json
      end

      it 'responds with unprocessable entity' do
        expect(last_response.status).to eq 422
      end

      it 'responds with specified error message' do
        expect(last_response.body)
          .to include 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$'
      end
    end

    context 'with duplicate shortcode' do
      before do
        post '/shorten', {
          url: attributes_for(:url).fetch(:original_url),
          shortcode: 'ABcdEF',
        }.to_json
      end

      it 'responds with conflict' do
        expect(last_response.status).to eq 409
      end

      it 'responds with specified error message' do
        expect(last_response.body).to include 'The the desired shortcode is already in use. Shortcodes are case-sensitive.'
      end
    end

    context 'when url parameter is not present' do
      before do
        post '/shorten', {}.to_json
      end

      it 'responds with bad request' do
        expect(last_response.status).to eq 400
      end

      it 'responds with specified error message' do
        expect(last_response.body).to include 'url is not present'
      end
    end
  end
end
