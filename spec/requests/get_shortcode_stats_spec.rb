require_relative '../spec_helper'

RSpec.describe 'Shortcode stats fetch', :type => :request do
  context 'when provided shortcode exists in the system' do
    let!(:url) { create(:url, shortcode: 'abcdef') }

    before do
      get "/#{url.shortcode}/stats"
    end

    it 'responds with ok' do
      expect(last_response.status).to eq 200
    end

    it 'has valid json response' do
      expect(last_response.body).to include_json({
        'startDate' => url.created_at.utc.iso8601,
        'redirectCount' => url.redirect_count
      })
    end

    context 'when redirect count is greater than 0' do
      before do
        url.update_attribute(:redirect_count, 2)
        get "/#{url.shortcode}/stats"
      end

      it 'has lastSeenDate in JSON response' do
        expect(last_response.body).to include_json({
          'startDate' => url.created_at.utc.iso8601,
          'redirectCount' => url.redirect_count,
          'lastSeenDate' => url.updated_at.utc.iso8601
        })
      end
    end
  end

  context "when provided shortcode doesn't exist in the database" do
    before do
      get '/abcdef/stats'
    end

    it 'responds with not found' do
      expect(last_response.status).to eq 404
    end

    it 'responds with predefined message' do
      expect(last_response.body).to include 'The shortcode cannot be found in the system'
    end
  end
end
