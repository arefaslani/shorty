require_relative '../spec_helper'

RSpec.describe 'Shortcode redirection', :type => :request do
  context 'when provided shortcode exists in the system' do
    let!(:url) { create(:url, shortcode: 'abcdef') }

    before do
      get '/abcdef'
    end

    it 'responds with found' do
      expect(last_response.status).to eq 302
    end

    it 'has valid location header' do
      expect(last_response.headers.fetch('Location')).to eq url.original_url
    end
  end

  context "when provided shortcode doesn't exist in the database" do
    before do
      get '/abcdef'
    end

    it 'responds with not found' do
      expect(last_response.status).to eq 404
    end

    it 'responds with predefined message' do
      expect(last_response.body).to include 'The shortcode cannot be found in the system'
    end
  end
end
