require_relative '../spec_helper'
require_relative '../../app/interactions/redirect_stats_manager'

describe RedirectStatsManager do
  describe '#run' do
    context 'with an existing shortcode' do
      let(:url) { create(:url) }
      it 'returns a valid outcome' do
        outcome = RedirectStatsManager.run(shortcode: url.shortcode)
        expect(outcome).to be_valid
      end

      it 'increments url redirect count by 1' do
        expect { RedirectStatsManager.run(shortcode: url.shortcode) }
          .to change { url.reload.redirect_count }.by 1
      end

      it 'returns the url with specified shortcode' do
        outcome = RedirectStatsManager.run(shortcode: url.shortcode)
        expect(outcome.result).to eq url
      end
    end

    context 'with non-existing shortcode' do
      it 'returns an invalid outcome' do
        outcome = RedirectStatsManager.run(shortcode: 'non-existing-code')
        expect(outcome).to be_invalid
      end

      it 'fills in the outcome errors' do
        outcome = RedirectStatsManager.run(shortcode: 'non-existing-code')
        expect(outcome.errors.details).to include :shortcode
      end
    end

    context 'without shortcode' do
      it 'returns an invalid outcome' do
        outcome = RedirectStatsManager.run
        expect(outcome).to be_invalid
      end

      it 'fills in the outcome errors' do
        outcome = RedirectStatsManager.run
        expect(outcome.errors.details).to include :shortcode
      end
    end
  end
end
