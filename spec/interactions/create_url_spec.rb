require_relative '../spec_helper'
require_relative '../../app/interactions/create_url'
require_relative '../../app/models/url'
require_relative '../../app/models/shortcode'

describe CreateUrl do
  describe '#run' do
    before { create_list(:shortcode, 3) }

    context 'with valid params' do
      it 'returns a valid output' do
        expect(described_class.run(attributes_for(:url))).to be_valid
      end

      it 'returns an output containing the created url' do
        expect(described_class.run(attributes_for(:url)).result)
          .to respond_to :shortcode
      end

      it 'adds a new url to the database' do
        expect { described_class.run(attributes_for(:url)) }
          .to change { Url.count }.by 1
      end
    end

    context 'with invalid params' do
      it 'returns an invalid output' do
        expect(described_class.run(attributes_for(:url, shortcode: '123')))
          .to be_invalid
      end

      it 'returns an output with errors' do
        expect(described_class.run(attributes_for(:url, shortcode: '123')).errors)
          .not_to be_empty
      end
    end

    context 'providing custom shortcode' do
      it 'is invalid with shortcode length is less that 4' do
        expect(described_class.run(attributes_for(:url, shortcode: '123')))
          .to be_invalid
      end

      it 'creates a new shortcode' do
        expect {
          described_class.run(attributes_for(:url, shortcode: 'new_shortcode'))
        }.to change { Shortcode.count }.by 1
      end

      it 'is invalid when shortcode contains any character other that 0-9, a-z, A-Z or _' do
        expect(described_class.run(attributes_for(:url, shortcode: '12345!')))
          .to be_invalid
      end

      it 'matches ^[0-9a-zA-Z_]{4,}$ regex' do
        outcome = described_class
                    .run(attributes_for(:url, shortcode: 'vAliD_Shortcode'))

        expect(outcome.result.shortcode).to eq 'vAliD_Shortcode'
      end

      it 'returns a new url with specified shortcode' do
        outcome = described_class
                    .run(attributes_for(:url, shortcode: 'valid_shortcode'))
        expect(outcome.result.shortcode)
          .to eq 'valid_shortcode'
      end

      context 'when preferred shortcode is already taken' do
        before { create(:url, shortcode: 'duplicate') }

        let(:outcome) do
          described_class.run(attributes_for(:url, shortcode: 'duplicate'))
        end

        it 'returns an invalid output' do
          expect(outcome).to be_invalid
        end

        it 'returns errors containing shortcode taken key' do
          expect(outcome.errors.added? :shortcode, :taken).to be_truthy
        end
      end

      context 'when preferred shortcode is empty' do
        it 'creates a random shorcode which matches ^[0-9a-zA-Z_]{6}$' do
          outcome = described_class
            .run(attributes_for(:url).reject { |key| key == :shortcode })

          expect(outcome.result.shortcode).to match /\A[0-9a-zA-Z_]{6}\z/
        end

        it 'labels a shortcode as used' do
          expect { described_class.run(attributes_for(:url, shortcode: nil)) }
            .to change { Shortcode.used.count }.by 1
        end

        it 'doesnt create a new shortcode' do
          expect { described_class.run(attributes_for(:url, shortcode: nil)) }
            .not_to change { Shortcode.count }
        end
      end
    end

    context 'without url param' do
      let(:output) do
        described_class
          .run(attributes_for(:url).reject { |key| key == :original_url })
      end

      it 'returns an invalid output' do
        expect(output).to be_invalid
      end

      it 'contains original url missing error' do
        expect(output.errors.added? :original_url, :missing).to be_truthy
      end
    end
  end
end
