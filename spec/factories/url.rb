require 'factory_bot'

FactoryBot.define do
  factory :url do
    original_url { 'https://google.com' }
    shortcode { '123145' }
  end
end
