require 'factory_bot'

FactoryBot.define do
  factory :shortcode do
    used { false }
    code { SecureRandom.base64(8).gsub(/(\/|\+)/,"").slice(0, 6) }
  end
end
