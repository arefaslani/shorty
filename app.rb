require 'sinatra/base'
require 'sinatra/reloader'
require './app/interactions/create_url'
require './app/interactions/redirect_stats_manager'

class App < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
  end

  before do
    content_type 'application/json'
  end

  post '/shorten' do
    outcome = CreateUrl.run(
      original_url: params.fetch('url', nil),
      shortcode: params.fetch('shortcode', nil)
    )

    if outcome.valid?
      status 201
      { shortcode: outcome.result.shortcode }.to_json
    elsif outcome.errors.added? :original_url, :missing
      status 400
      { url: ['url is not present'] }.to_json
    elsif outcome.errors.added? :shortcode, :taken
      status 409
      { shortcode: ['The the desired shortcode is already in use. Shortcodes are case-sensitive.'] }.to_json
    else
      status 422
      outcome.errors.to_json
    end
  end

  get '/:shortcode' do |shortcode|
    outcome = RedirectStatsManager.run(shortcode: shortcode)

    if outcome.valid?
      status 302
      return response.headers['Location'] = outcome.result.original_url
    end

    status 404
    outcome.errors.to_json
  end

  get '/:shortcode/stats' do |shortcode|
    url = Url.find_by(shortcode: shortcode)
    if url
      status 200
      response = {
        'startDate': url.created_at.utc.iso8601,
        'redirectCount': url.redirect_count
      }.tap do |r|
        r.merge!('lastSeenDate': url.updated_at.utc.iso8601) if url.redirect_count > 0
      end
      return response.to_json
    end

    status 404
    return { shortcode: ['The shortcode cannot be found in the system'] }.to_json
  end

  private

  def params
    @parameters ||= JSON.parse(request.body.read)
  rescue JSON::ParserError
    {}
  end
end
