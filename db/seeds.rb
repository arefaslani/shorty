require 'securerandom'

require_relative '../app/models/shortcode'

def generate_code
  SecureRandom.base64(8).gsub(/(\/|\+)/,"").slice(0, 6)
end

300.times do
  Shortcode.create!(code: generate_code, used: false)
rescue ActiveRecord::RecordNotUnique
  next
end
