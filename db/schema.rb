# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_03_152558) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "shortcodes", force: :cascade do |t|
    t.string "code"
    t.boolean "used", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_shortcodes_on_code", unique: true
    t.index ["used"], name: "index_shortcodes_on_used"
  end

  create_table "urls", force: :cascade do |t|
    t.string "original_url", null: false
    t.string "shortcode"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "redirect_count", default: 0
    t.index ["original_url"], name: "index_urls_on_original_url"
    t.index ["shortcode"], name: "index_urls_on_shortcode", unique: true
  end

end
