class CreateShortcodes < ActiveRecord::Migration[6.0]
  def change
    create_table :shortcodes do |t|
      t.string :code
      t.boolean :used, index: true, default: false

      t.timestamps
    end

    add_index :shortcodes, :code, unique: true
  end
end
