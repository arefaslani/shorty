class AddRedirectCountToUrls < ActiveRecord::Migration[6.0]
  def change
    add_column :urls, :redirect_count, :integer, default: 0
  end
end
