class CreateUrls < ActiveRecord::Migration[6.0]
  def change
    create_table :urls do |t|
      t.string :original_url, null: false, index: true
      t.string :shortcode

      t.timestamps
    end

    add_index :urls, :shortcode, unique: true
  end
end
